# List all block devices excluding CD/DVD drives
ALL_DISKS=$(lsblk -dno NAME | grep -v 'sr')

# Initialize an empty string to hold unmounted disks
UNMOUNTED_DISKS=""

# Check each disk to see if it's mounted
for DISK in $ALL_DISKS; do
    DISK_PATH="/dev/$DISK"
    if ! mount | grep -q $DISK_PATH && ! pvdisplay $DISK_PATH &>/dev/null; then
        UNMOUNTED_DISKS+="$DISK_PATH "
    fi
done

# Output the list of unmounted disks
if [ -z "$UNMOUNTED_DISKS" ]; then
    echo "No unmounted disks found or all disks are already in use."
    exit 0
fi

diskSize=$1
driveLetter=$2

# Convert JSON-like input to Bash arrays
trimmedStr=${driveLetter#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }
read -a arrayLetter <<< "$formattedStr"

trimmedStr=${diskSize#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }
read -a arraySize <<< "$formattedStr"

read -a arrayDevices <<< "$UNMOUNTED_DISKS"

i=0
for drive in "${arrayLetter[@]}"; do
    if [ -z "${arrayDevices[i]}" ]; then
        echo "Not enough unmounted disks available."
        break
    fi
    echo "Preparing $drive with size ${arraySize[i]}GB on ${arrayDevices[i]}"

    # Use fdisk to create a new partition of type 8e (Linux LVM)
    echo -e "o\nn\np\n1\n\n\nt\n8e\nw" | fdisk ${arrayDevices[i]}
    PARTITION_PATH="${arrayDevices[i]}1"

    # Initialize Physical Volume, Create Volume Group, and Logical Volume
    pvcreate $PARTITION_PATH
    BASE_NAME=$(basename $drive)
    VG_NAME="${BASE_NAME}vg"
    LV_NAME="${BASE_NAME}lv"
    vgcreate $VG_NAME $PARTITION_PATH
    lvcreate -l 100%FREE -n $LV_NAME $VG_NAME

    # Format the Logical Volume with XFS
    mkfs.xfs /dev/$VG_NAME/$LV_NAME

    # Mount and update fstab
    mkdir -p $drive
    mount /dev/$VG_NAME/$LV_NAME $drive
    echo "/dev/$VG_NAME/$LV_NAME $drive xfs defaults,noatime 0 0" >> /etc/fstab

    if [[ ${#arrayLetter[@]} == 1 ]]; then
        break
    fi

    ((i++))
done