#!/bin/bash

# List all block devices excluding CD/DVD drives
ALL_DISKS=$(lsblk -dno NAME | grep -v 'sr')

# Initialize an empty string to hold unmounted disks
UNMOUNTED_DISKS=""

# Check each disk to see if it's mounted
for DISK in $ALL_DISKS; do
    # Using /dev/ prefix to match device file path
    DISK_PATH="/dev/$DISK"
    # Check if the disk is not in the mount list and not part of LVM
    if ! mount | grep -q $DISK_PATH && ! pvdisplay $DISK_PATH &>/dev/null; then
        # If not mounted and not part of LVM, add to the list
        UNMOUNTED_DISKS+="$DISK_PATH "
    fi
done

# Output the list of unmounted disks
if [ -z "$UNMOUNTED_DISKS" ]; then
    echo "No unmounted disks found or all disks are already in use."
    exit 0
fi

diskSize=$1
driveLetter=$2

# Convert JSON-like input to Bash arrays
trimmedStr=${driveLetter#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }
read -a arrayLetter <<< "$formattedStr"

trimmedStr=${diskSize#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }
read -a arraySize <<< "$formattedStr"

read -a arrayDevices <<< "$UNMOUNTED_DISKS"

i=0
for drive in "${arrayLetter[@]}"; do
    if [ -z "${arrayDevices[i]}" ]; then
        echo "Not enough unmounted disks available."
        break
    fi
    echo "Preparing $drive with size ${arraySize[i]}GB on ${arrayDevices[i]}"

    # Initialize Physical Volume
    pvcreate ${arrayDevices[i]}
    # Create Volume Group
    vgcreate vg${i} ${arrayDevices[i]}
    # Create Logical Volume
    lvcreate -l 100%FREE -n lv${i} vg${i}

    # Format the Logical Volume with XFS
    mkfs.xfs /dev/vg${i}/lv${i}

    # Create the mount point directory
    mkdir -p $drive
    # Mount the Logical Volume
    mount /dev/vg${i}/lv${i} $drive
    # Add the mount to fstab for automatic remounting on reboot
    echo "/dev/vg${i}/lv${i} $drive xfs defaults,noatime 0 0" >> /etc/fstab

    if [[ ${#arrayLetter[@]} == 1 ]]; then
        break
    fi

    ((i++))
done
