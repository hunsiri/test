#!/bin/bash

# List all block devices excluding CD/DVD drives
ALL_DISKS=$(lsblk -dno NAME | grep -v 'sr')

# Initialize an empty string to hold unmounted disks
UNMOUNTED_DISKS=""

# Check each disk to see if it's mounted
for DISK in $ALL_DISKS; do
    # Using /dev/ prefix to match device file path
    DISK_PATH="/dev/$DISK"
    # Check if the disk is not in the mount list and not part of LVM
    if ! mount | grep -q $DISK_PATH && ! pvdisplay $DISK_PATH &>/dev/null; then
        # If not mounted and not part of LVM, add to the list
        UNMOUNTED_DISKS+="$DISK_PATH "
    fi
done

# Output the list of unmounted disks
if [ -z "$UNMOUNTED_DISKS" ]; then
    echo "No unmounted disks found or all disks are already in use."
    exit 0
fi

diskSize=$1
driveLetter=$2

# Convert JSON-like input to Bash arrays
trimmedStr=${driveLetter#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }
read -a arrayLetter <<< "$formattedStr"

trimmedStr=${diskSize#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }
read -a arraySize <<< "$formattedStr"

read -a arrayDevices <<< "$UNMOUNTED_DISKS"

i=0
for drive in "${arrayLetter[@]}"; do
    if [ -z "${arrayDevices[i]}" ]; then
        echo "Not enough unmounted disks available."
        break
    fi
    echo "Preparing $drive with size ${arraySize[i]}GB on ${arrayDevices[i]}"

    # Create a partition using parted with lvm flag
    parted ${arrayDevices[i]} --script mklabel gpt
    parted ${arrayDevices[i]} --script mkpart primary ext4 0% 100%
    parted ${arrayDevices[i]} --script set 1 lvm on
    partprobe ${arrayDevices[i]}

    # Get the partition path (usually it will be ${arrayDevices[i]}1)
    PARTITION_PATH="${arrayDevices[i]}1"

    # Extract the name from the drive variable for VG and LV names
    BASE_NAME=$(basename $drive)
    VG_NAME="${BASE_NAME}vg"
    LV_NAME="${BASE_NAME}lv"

    # Initialize Physical Volume
    pvcreate $PARTITION_PATH
    # Create Volume Group
    vgcreate $VG_NAME $PARTITION_PATH
    # Create Logical Volume
    lvcreate -l 100%FREE -n $LV_NAME $VG_NAME

    # Format the Logical Volume with XFS
    mkfs.xfs /dev/$VG_NAME/$LV_NAME

    # Create the mount point directory
    mkdir -p $drive
    # Mount the Logical Volume
    mount /dev/$VG_NAME/$LV_NAME $drive
    # Add the mount to fstab for automatic remounting on reboot
    echo "/dev/$VG_NAME/$LV_NAME $drive xfs defaults,noatime 0 0" >> /etc/fstab

    if [[ ${#arrayLetter[@]} == 1 ]]; then
        break
    fi

    ((i++))
done