# vRA assembly
# Set-Variable -Name "driveLetter" -Value '${input.DISKs.diskPath}'
# Set-Variable -Name "diskSize" -Value '${input.DISKs.capacityGb}'
# Set-Variable -Name "driveLabel" -Value '${input.additional_drive_label}'

$arraySize = $diskSize | ConvertFrom-Json
$arrayLetter = $driveLetter | ConvertFrom-Json
$countLoop = $arraySize.Count
$countIndex = 0

while($countIndex -le $countLoop)
{
        echo $arraySize[$countIndex]
        Get-Disk | Where partitionstyle -eq 'raw' | select -First 1 | Initialize-Disk -PartitionStyle GPT -PassThru | New-Partition -DriveLetter $arrayLetter[$countIndex] -UseMaximumSize | Format-Volume -FileSystem NTFS -AllocationUnitSize 65536 -Confirm:$false

        ### with DiskLabel
        # Get-Disk | Where partitionstyle -eq 'raw' | select -First 1 | Initialize-Disk -PartitionStyle GPT -PassThru | New-Partition -DriveLetter $arrayLetter[$countIndex] -UseMaximumSize | Format-Volume -FileSystem NTFS -NewFileSystemLabel $arrayLabel[$countIndex] -AllocationUnitSize 65536 -Confirm:$false
        $countIndex = $countIndex + 1
}