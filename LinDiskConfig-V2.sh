#!/bin/bash


# List all block devices
ALL_DISKS=$(lsblk -dno NAME |grep -v 'sr')

# Initialize an empty string to hold unmounted disks
UNMOUNTED_DISKS=""

# Check each disk to see if it's mounted
for DISK in $ALL_DISKS; do
    # Using /dev/ prefix to match device file path
    DISK_PATH="/dev/$DISK"
    # Check if the disk is not in the mount list
    if ! mount | grep -q $DISK_PATH; then
        # If not mounted, add to the list
        UNMOUNTED_DISKS+="$DISK_PATH "
    fi
done

# Output the list of unmounted disks
if [ -n "$UNMOUNTED_DISKS" ]; then
    echo "Unmounted Disks: $UNMOUNTED_DISKS"
else
    echo "No unmounted disks found."
fi


# export diskSize='[1,3,5]'
# export driveLetter='["/disk1","/disk2","/disk3"]'

diskSize=$1
driveLetter=$2

trimmedStr=${driveLetter#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }

read -a arrayLetter <<< "$formattedStr"

trimmedStr=${diskSize#[}
trimmedStr=${trimmedStr%]}
formattedStr=${trimmedStr//\"/}
formattedStr=${formattedStr//,/ }

read -a arraySize <<< "$formattedStr"

read -a arrayDevices <<< "$UNMOUNTED_DISKS"


i=0
for drive in "${arrayLetter[@]}"; do
    echo "$drive"
    echo "${arraySize[i]}"
    echo "${arrayDevices[i]}"
    mkfs.xfs ${arrayDevices[i]}
    mkdir $drive
    mount ${arrayDevices[i]} $drive
    echo "${arrayDevices[i]}    $drive  xfs     defaults,noatime    0 0" >> /etc/fstab

    ((i++))
done
